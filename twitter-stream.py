# Import tweepy, the Python wrapper for the Twitter API.
# Tweepy is what allows us to communicate with Twitter.
import tweepy

# Import json, which allows us to decode the data received
# from the Twitter API (most APIs use either JSON or XML).
import json

# NOTE: In order to use this program you will need to obtain auth keys from your
# own Twitter account, then paste them in here.
access_token = ''
access_token_secret = ''
consumer_key = ''
consumer_secret = ''

# Set up the listener, which "listens" to tweets being published to Twitter
class StdOutListener(tweepy.StreamListener):

    # Define what happens when we receive data from Twitter
    def on_data(self, data):

        # Here we 'decode' the data from Twitter, making use of json
        decoded = json.loads(data)
	
	# We then locate specific parts of the Tweet, namely the user's name
	# and message of the tweet, and assign them to variables.
	userName = decoded['user']['screen_name']
	userTweetRaw = decoded['text'].encode('ascii', 'ignore') # This last method filters out any 'bad' characters
	userTweet = ' '.join(userTweetRaw.split()) # This method strips all whitespace from the message

	# Now we print out a message with the username and tweet
	print '\n(+1) New tweet by @' + userName + ':'
	print '\t' + userTweet

        return True
    
    # This function handles any errors, printing the status if any occur
    def on_error(self, status):
        print status

# Define the main function
def main():

    # Here we instantiate our listener and set up authentication with Twitter
    listener = StdOutListener()
    auth = tweepy.OAuthHandler(consumer_key, consumer_secret)
    auth.set_access_token(access_token, access_token_secret)

    # Print a welcome message before we start dealing with user input
    print '\nWelcome to the Live Twitter Streamer'

    # Declare userInput and assign a value of 0
    # for error handling purposes (below).
    userInput = 0

    # Here I've added some error handling; if a user does not
    # enter anything into the prompt, we print an error and
    # reset userInput to 0 to let them try again.
    while userInput == 0:
	userInput = raw_input('Enter a search query to track: ')

        if userInput == '':
	    print 'Please enter a valid query.\n'
	    userInput = 0

    # Declare listenQuery and assign the value of userInput.
    # This is really just to make things more readable,
    # we could use userInput everywhere if we wanted to.
    listenQuery = userInput

    # Print out a starting message before we start listing new tweets
    print '\nShowing all new tweets for ' + listenQuery + ':'

    # Set up the stream with authentication and our listener to track our query
    stream = tweepy.Stream(auth, listener)
    stream.filter(track=[listenQuery])

# Call the main function
main()
