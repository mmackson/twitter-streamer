# twitter-streamer

Twitter Streamer is a simple keyword-based Twitter streamer built using Python and Tweepy. In order to use this program, authentication keys will need to be obtained from [https://apps.twitter.com/](https://apps.twitter.com/) and pasted into twitter-stream.py after the appropriate variables.